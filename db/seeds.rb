# frozen_string_literal: true

require "factory_bot_rails"

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Tournament.delete_all
Team.delete_all

tournaments = FactoryBot.create_list(:tournament, 5)

# HABTM stuff
teams = FactoryBot.create_list(:team, 16, tournaments: [tournaments.sample])

# some winners & finalists
tournaments.shuffle.sample(2).each do |t|
  t.winner = teams.sample
end

tournaments.shuffle.sample(2).each do |t|
  t.finalist = teams.sample
end
