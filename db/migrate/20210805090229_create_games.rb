# frozen_string_literal: true

class CreateGames < ActiveRecord::Migration[5.2]
  def change
    create_table :games do |t|
      t.references(:tournament, foreign_key: true)
      t.integer(:aasm_state)
      t.integer(:team1_id)
      t.integer(:team2_id)
      t.integer(:winner_id, index: true)

      t.timestamps
    end
  end
end
