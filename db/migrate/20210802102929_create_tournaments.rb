# frozen_string_literal: true

class CreateTournaments < ActiveRecord::Migration[5.2]
  def change
    create_table :tournaments do |t|
      t.string(:name, null: false)
      t.integer(:aasm_state, index: true)
      t.integer(:winner_id)
      t.integer(:finalist_id)

      t.timestamps

      t.index(:name, unique: true)
    end
  end
end
