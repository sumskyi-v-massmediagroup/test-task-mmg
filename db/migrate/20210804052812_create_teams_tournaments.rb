# frozen_string_literal: true

class CreateTeamsTournaments < ActiveRecord::Migration[5.2]
  def change
    create_table :teams_tournaments, id: false do |t|
      t.belongs_to(:team)
      t.belongs_to(:tournament)
      t.timestamps

      t.index([:team_id, :tournament_id], unique: true)
    end
  end
end
