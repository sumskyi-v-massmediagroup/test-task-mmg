# frozen_string_literal: true

require "rails_helper"

RSpec.describe PlayService do
  subject(:service) { described_class.new(tournament) }

  let(:tournament) { create(:tournament) }

  describe "1st play" do
    # before { service.process }
  end

  describe PlayService::Stage::Init do
    subject(:stage) { described_class.new(group, tournament) }

    let!(:group) do
      create_list(
        :team,
        PlayService::GROUP_SIZE,
        tournaments: [tournament]
      )
    end
    let(:pairs_a) { stage.pairs_a }
    let(:pairs_b) { stage.pairs_b }

    before { stage.send(:set_pairs) }

    it "combines group teams to play with every team in a command" do
      expect(pairs_a.size).to be_equal(28)
      expect(pairs_b.size).to be_equal(28)
    end
  end
end
