# frozen_string_literal: true

require "rails_helper"

RSpec.describe Tournament, type: :model do
  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:aasm_state) }
  end

  describe "associations" do
    it { is_expected.to have_and_belong_to_many(:teams) }
  end

  context "with data" do
    let!(:tournament) { create(:tournament, name: "invalid") }
    let(:invalid) { build(:tournament, name: "invalid") }

    it "not allows the same name" do
      expect(invalid).to_not be_valid
    end

    describe "teams in the tournament" do
      before { tournament.teams << team }

      let(:team) { create(:team) }

      it "#teams_count" do
        expect(tournament.teams_count).to be_equal(1)
      end
    end

    describe "#playable?" do
      it "allows when right teams count" do
        allow(tournament).to receive(:teams_count).and_return(PlayService::GROUP_SIZE)
        allow(tournament).to receive(:done?).and_return(false)
        expect(tournament).to be_playable
      end

      it "not allows when teams count is not enough" do
        allow(tournament).to receive(:teams_count).and_return(0)
        allow(tournament).to receive(:done?).and_return(false)
        expect(tournament).to_not be_playable
      end
    end
  end
end
