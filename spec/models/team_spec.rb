# frozen_string_literal: true

require "rails_helper"

RSpec.describe Team, type: :model do
  it { is_expected.to have_and_belong_to_many(:tournaments) }
  it { is_expected.to validate_presence_of(:name) }

  context "with data" do
    let!(:tournament) { create(:tournament) }
    let!(:team) { create(:team, tournaments: [tournament]) }

    it { expect(team).to be_valid }
  end

  context "given validations" do
    let!(:tournament) { create(:tournament) }
    let!(:group) { create_list(:team, 16, tournaments: [tournament]) }
    let(:extra_team) { build(:team) }

    before { extra_team.current_tournament = tournament }

    it "don't allows more than 16 teams per group" do
      expect { extra_team.save! }.to raise_error(::ActiveModel::ValidationError)
    end
  end
end
