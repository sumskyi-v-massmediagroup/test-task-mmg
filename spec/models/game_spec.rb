# frozen_string_literal: true

require "rails_helper"

RSpec.describe Game, type: :model do
  describe "associations & validations" do
    it { is_expected.to belong_to(:tournament) }
  end
end
