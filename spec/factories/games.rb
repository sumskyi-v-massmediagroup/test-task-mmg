# frozen_string_literal: true

FactoryBot.define do
  factory :game do
    tournament { nil }
    aasm_state { %i[init groups playoff finished].sample }
    team1_id { nil }
    team2_id { nil }
    winner_id { nil }
  end
end
