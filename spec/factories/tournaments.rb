# frozen_string_literal: true

FactoryBot.define do
  factory :tournament do
    sequence(:name) { |n| "My Name #{ n }" }
    aasm_state { %i[draft in_progress done].sample }
    winner { false }
    finalist { false }
  end
end
