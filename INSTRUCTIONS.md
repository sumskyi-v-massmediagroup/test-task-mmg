# Ruby on Rails developer task
## _Mogo Finance_
[![N|Solid](./mogo_finance.jpg)](https://eleving.com/)
### Docker
It's very easy to install and deploy in a Docker container.

By default, the Docker will expose port 3000, so change this within the
Dockerfile if necessary. When ready, simply use the Dockerfile to
build the image.

```sh
1. docker-compose run --no-deps web rails new . --force --database=mysql
2. docker-compose run web bundle install
3. chown -R $USER:$USER .
4. git checkout HEAD README.md
5. docker-compose build
6. docker-compose up
7. rm -f db/schema.rb
8. docker-compose exec web rake db:drop
9. docker-compose exec web rake db:create
10.docker-compose exec web rake db:migrate
11. chown $USER:$USER db/schema.rb
12. docker-compose exec web rake db:seed
```

Verify the deployment by navigating to your server address in
your preferred browser.

```sh
127.0.0.1:3000
```
