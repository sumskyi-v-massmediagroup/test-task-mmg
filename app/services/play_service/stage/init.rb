# frozen_string_literal: true

class PlayService
  module Stage
    class Init < Base
      attr_reader :pairs_a, :pairs_b

      BEST_COUNT = 4

      def initialize(*args)
        @stage = :init
        super
      end

      private

        def set_pairs
          @group_a, @group_b = @group.each_slice(GROUP_SIZE / 2).to_a

          @pairs_a = calculate_pairs(@group_a)
          @pairs_b = calculate_pairs(@group_b)
        end

        def play_pairs
          @pairs_a.each(&write_proc)
          @pairs_b.each(&write_proc)
        end

        def result
          ids = @group_a.map(&:id)
          winners1 = Game.best_in_group(ids).take(BEST_COUNT)

          ids = @group_b.map(&:id)
          winners2 = Game.best_in_group(ids).take(BEST_COUNT)

          [winners1, winners2]
        end

        def calculate_pairs(arr)
          pairs = Set.new

          # rubocop:disable Style/RedundantSortBy
          #
          # sort_by. It implements a Schwartzian transform which is useful
          #   when key computation or comparison is expensive.
          arr.each do |i|
            arr.each do |j|
              next if i.id == j.id

              pairs << [i.id, j.id].sort_by { |el| el }
            end
          end
          # rubocop:enable Style/RedundantSortBy

          pairs
        end
    end
  end
end
