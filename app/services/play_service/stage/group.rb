# frozen_string_literal: true

class PlayService
  module Stage
    # each team within the group
    # plays with others
    # results are stored in the database
    class Group < Base
      def initialize(*args)
        @stage = :groups
        super
      end

      private

        def set_pairs
          @ordered_ids = Game.best_in_group(payload)
        end

        # best vs. worst
        def play_pairs
          best = @ordered_ids[0]
          worst = @ordered_ids[-1]

          if best == worst
            @best = best
            return best
          end

          game = write([best, worst])

          # just XOR
          except_winner = @ordered_ids - [game.winner_id]
          played = [best, worst]
          except_looser = Set.new(except_winner) ^ played

          # raise except_looser.to_s
          @ordered_ids =
            @ordered_ids.delete_if do |el|
              except_looser.exclude?(el)
            end

          # minor recursion
          play_pairs
        end

        def result
          @best
        end
    end
  end
end
