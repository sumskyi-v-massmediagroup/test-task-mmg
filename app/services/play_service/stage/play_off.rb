# frozen_string_literal: true

class PlayService
  module Stage
    # Play-off initial schedule is made by principle 'the best team plays against the worst team
    # The winning team stays to play further, but the losing team is out of the game.
    # Overall winning team is the one WHO WINS ALL GAMES in play-off.
    class PlayOff < Base
      def initialize(*args)
        @stage = :playoff
        super
      end

      private

        def set_pairs
          @ids = payload
        end

        def play_pairs
          @game = write(payload)
        end

        def result
          @game.winner_id
        end
    end
  end
end
