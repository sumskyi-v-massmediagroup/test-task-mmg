# frozen_string_literal: true

class PlayService
  module Stage
    class Base
      attr_accessor :tournament, :group, :payload
      attr_reader :stage

      def initialize(group, tournament, payload=nil)
        @group = group
        @tournament = tournament
        @payload = payload
      end

      def run
        set_pairs
        play_pairs
        result
      end

      private

        def write(pair)
          Game.create!(
            tournament: tournament,
            stage: stage,
            team1_id: pair[0],
            team2_id: pair[1],
            winner_id: pair.sample
          )
        end

        def write_proc
          @_write_proc ||= method(:write)
        end

        def set_pairs
          raise NoMethodError.new("set_pairs. Implement it in #{ self.class }")
        end

        def play_pairs
          raise NoMethodError.new("play_pairs. Implement it in #{ self.class }")
        end

        def result
          raise NoMethodError.new("result. Implement it in #{ self.class }")
        end
    end
  end
end
