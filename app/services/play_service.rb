# frozen_string_literal: true

class PlayService
  GROUP_SIZE = 16

  attr_reader :tournament, :group

  WrongTeamSize = Class.new(StandardError)

  def initialize(tournament)
    @tournament = tournament

    # group consists of randomized teams
    @group = @tournament.teams.shuffle
    @success = true

    if GROUP_SIZE != @group.size
      raise WrongTeamSize.new("Group size must be #{ GROUP_SIZE }")
    end
  rescue StandardError => e
    Rails.logger.error(e.backtrace)
    @success = false
    reset_games!

    raise
  end

  def play!
    best4 = execute(Stage::Init)

    playoff_winner1 = execute(Stage::Group, best4[0])
    playoff_winner2 = execute(Stage::Group, best4[1])

    finalist_id = execute(Stage::PlayOff, [playoff_winner1, playoff_winner2])

    tournament.update!(winner_id: finalist_id, finalist_id: finalist_id)
    tournament.done!
  end

  def success?
    @success
  end

  def error?
    !@success
  end

  # TBD
  def error
    tournament.errors.add(:base, "Something went wrong. Play again.")
  end

  private

    def execute(klass, payload=nil)
      stage = klass.new(group, tournament, payload)
      stage.run
    end

    # Cleanup tournament results
    def reset_games!
      tournament.games.delete_all
    end
end
