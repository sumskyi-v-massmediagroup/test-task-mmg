# frozen_string_literal: true

class TournamentsController < ApplicationController
  expose :tournaments, -> { Tournament.all }
  expose :tournament
  expose :teams_array, -> { tournament.teams }

  def index; end

  def show; end

  def new; end

  def edit; end

  # GET
  def play_index
    render :play_index
  end

  # POST
  def play_start
    service = PlayService.new(tournament)
    service.play!

    redirect_to results_tournament_path(tournament), \
      notice: "Tournament was successfully finished."
  rescue StandardError => e
    tournament.errors.add(:base, e.message)
    tournament.errors.add(:base, e.backtrace.first)

    render :play_index
  end

  def results; end

  def create
    if tournament.save
      redirect_to tournament_path(tournament), \
        notice: "Tournament was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if tournament.update(tournament_params)
      redirect_to tournament, notice: \
        "Tournament was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    tournament.destroy!
    redirect_to tournaments_url, notice: \
      "Tournament was successfully destroyed."
  end

  private

    # Only allow a list of trusted parameters through.
    def tournament_params
      params.require(:tournament).permit(:name, :status, :winner, :finalist)
    end
end
