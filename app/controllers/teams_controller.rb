# frozen_string_literal: true

class TeamsController < ApplicationController
  expose :tournaments_array, -> { Tournament.to_a }
  expose :tournament, -> { Tournament.find(tournament_id) }
  expose :teams, -> do
    if params[:tournament_id]
      tournament.teams
    else
      Team.all
    end
  end

  expose :team

  def index; end

  def by_tournament
    render :index
  end

  def show; end

  def new; end

  def edit; end

  def create
    team.current_tournament = tournament
    if team.save
      team.tournaments << tournament
      redirect_to team, notice: "Team was successfully created."
    else
      render :new, status: :unprocessable_entity
    end
  rescue ActiveModel::ValidationError => e
    render :new, status: :unprocessable_entity, message: e
  rescue ActiveRecord::RecordNotUnique => e
    team.errors.add(:name, "is already exists")
    render :new, status: :unprocessable_entity, message: e
  end

  def update
    team.current_tournament = tournament
    if team.update(team_params)
      team.tournaments << tournament
      redirect_to team, notice: "Team was successfully updated."
    else
      render :edit, status: :unprocessable_entity
    end
  rescue ActiveModel::ValidationError => e
    render :new, status: :unprocessable_entity, message: e
  rescue ActiveRecord::RecordNotUnique => e
    team.errors.add(:name, "is already exists in this tournament")
    render :new, status: :unprocessable_entity, message: e
  end

  def destroy
    team.destroy!
    redirect_to teams_url, notice: "Team was successfully destroyed."
  end

  private

    def tournament_id
      params[:tournament_id] || params[:team][:tournament_id]
    end

    # Only allow a list of trusted parameters through.
    def team_params
      params.require(:team).
        permit(:name, tournaments_attributes: [:id, :tournament_id])
    end
end
