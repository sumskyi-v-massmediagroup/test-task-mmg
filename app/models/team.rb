# frozen_string_literal: true

class Team < ApplicationRecord
  attr_accessor :current_tournament

  # rubocop:disable Rails/HasAndBelongsToMany
  has_and_belongs_to_many :tournaments
  # rubocop:enable Rails/HasAndBelongsToMany

  belongs_to :winner_tournament,
    class_name: "Tournament",
    inverse_of: :winner,
    primary_key: :winner_id,
    foreign_key: :id,
    dependent: nil,
    required: false

  belongs_to :finalist_tournament,
    class_name: "Tournament",
    inverse_of: :finalist,
    primary_key: :finalist_id,
    foreign_key: :id,
    dependent: nil,
    required: false

  accepts_nested_attributes_for :tournaments

  validates :name, presence: true

  class GroupSizeValidator < ActiveModel::Validator
    def validate(record)
      tournament = record.current_tournament
      return false unless tournament

      if tournament.teams.count >= PlayService::GROUP_SIZE
        record.errors.add(:name, "Teams count (#{ PlayService::GROUP_SIZE }) exceeded")
        raise ::ActiveModel::ValidationError.new(record)
      else
        true
      end
    end
  end
  validates_with GroupSizeValidator
end
