# frozen_string_literal: true

class Tournament < ApplicationRecord
  include AASM

  aasm do
    state :draft, initial: true
    state :in_progress
    state :done
  end

  # rubocop:disable Rails/UniqueValidationWithoutIndex
  validates :name, presence: true, uniqueness: true
  # rubocop:enable Rails/UniqueValidationWithoutIndex

  alias_attribute :status, :aasm_state
  enum aasm_state: {draft: 0, in_progress: 1, done: 2}

  validates :aasm_state, presence: true

  has_many :games, dependent: :destroy

  # rubocop:disable Rails/HasAndBelongsToMany
  has_and_belongs_to_many :teams
  # rubocop:enable Rails/HasAndBelongsToMany

  has_one :winner,
    dependent: :nullify,
    class_name: "Team",
    primary_key: :winner_id,
    foreign_key: :id,
    inverse_of: :winner_tournament

  has_one :finalist,
    dependent: :nullify,
    class_name: "Team",
    primary_key: :finalist_id,
    foreign_key: :id,
    inverse_of: :finalist_tournament

  delegate :count, to: :teams, prefix: true

  def full_group?
    PlayService::GROUP_SIZE == teams_count
  end

  def playable?
    full_group? && !done?
  end

  # rubocop:disable Rails/SkipsModelValidations
  def winner=(team)
    return unless team

    update_column(:winner_id, team.id)
  end

  def finalist=(team)
    return unless team

    update_column(:finalist_id, team.id)
  end
  # rubocop:enable Rails/SkipsModelValidations

  def self.to_a
    all.each_with_object([]) do |t, memo|
      memo << [t.name, t.id]
    end
  end
end
