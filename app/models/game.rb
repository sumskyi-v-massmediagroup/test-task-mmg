# frozen_string_literal: true

class Game < ApplicationRecord
  include AASM

  aasm do
    state :init, initial: true
    state :groups
    state :playoff
    state :finished
  end
  alias_attribute :stage, :aasm_state
  enum aasm_state: {
    init: 0,
    groups: 1,
    playoff: 2,
    finished: 3
  }

  belongs_to :tournament

  # rubocop:disable Rails/InverseOf
  has_one :team1,
    dependent: :nullify,
    class_name: "Team",
    primary_key: :team1_id,
    foreign_key: :id

  has_one :team2,
    dependent: :nullify,
    class_name: "Team",
    primary_key: :team2_id,
    foreign_key: :id

  has_one :winner,
    dependent: :nullify,
    class_name: "Team",
    primary_key: :winner_id,
    foreign_key: :id
  # rubocop:enable Rails/InverseOf

  def self.best_in_group(ids, order=:desc)
    where(winner_id: ids).
      group(:winner_id).
      order(count_all: order).
      count.
      map { |id, _| id }
  end

  # rubocop:disable Rails/SkipsModelValidations
  def team1=(team)
    return unless team

    update_column(:team1_id, team.id)
  end

  def team2=(team)
    return unless team

    update_column(:team2_id, team.id)
  end

  def winner=(team)
    return unless team

    update_column(:winner_id, team.id)
  end
  # rubocop:enable Rails/SkipsModelValidations
end
