# frozen_string_literal: true

Rails.application.routes.draw do
  resources :teams
  resources :tournaments do
    member do
      get :play_index
      post :play_start
      get :results
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root "tournaments#index"

  get "/tournaments/:tournament_id/teams", \
    to: "teams#by_tournament", as: "tournament_teams"
end
